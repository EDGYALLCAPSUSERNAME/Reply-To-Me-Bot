import praw
import random
import time
import urllib2

REDDIT_USERNAME = ''
REDDIT_PASS = ''

USER_TO_REPLY_TO = ''

REPLIED_COMMENTS = []
REPLY = """
PUT REPLY HERE
"""

def main():
    print "Logging in..."
    r = praw.Reddit(user_agent = 'ReplyToMe v0.1')
    r.login(REDDIT_USERNAME, REDDIT_PASS, disable_warning = True)

    while True:
        while True:
            try:
                user = r.get_redditor(USER_TO_REPLY_TO)
                break
            except urllib2.HTTPError, e:
                if e.code in [429, 500, 502, 503, 504]:
                    print "Reddit is down (Error: {}), sleeping...".format(e.code)
                    time.sleep(60)
                    pass

        infile = open("comments.txt", 'r')
        REPLIED_COMMENTS = infile.read().splitlines()
        infile.close()

        while True:
            try:
                comments = user.get_comments(sort = 'new', limit = 10)
                break
            except urllib2.HTTPError, e:
                if e.code in [429, 500, 502, 503, 504]:
                    print "Reddit is down (Error: {}), sleeping...".format(e.code)
                    time.sleep(60)
                    pass

        for comment in comments:
            if not str(comment.id) in REPLIED_COMMENTS:
                try:
                    print "Replying to comment..."
                    comment.reply(REPLY)
                    REPLIED_COMMENTS.append(str(comment.id))
                except praw.errors.InvalidComment:
                    print "Comment no longer exists continuing..."


        infile = open("comments.txt", 'w')
        infile.seek(0)
        infile.truncate()

        for comment_id in REPLIED_COMMENTS:
            infile.write(comment_id + "\n")

        infile.close()

        # Wait 30 minutes
        time.sleep(1200)

if __name__ == "__main__":
    main()
