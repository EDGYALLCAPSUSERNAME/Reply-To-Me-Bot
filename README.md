Reply To Me Bot
===============

Setup
-----

This script requires python 2.7 and praw.

To install praw type in your command line:

    pip install praw

Create a blank comments.txt file, this will store the comments the bot has already
replied to, so it doesn't spam comments if it crashes.

After that open up the reply_to_me_bo.py file and edit the
REDDIT_USERNAME and REDDIT_PASS with the username and password of the account you want to post.

Change the Reply you want it to send and set up the user you want it to send it to.

**Please don't use this to spam people.**
